# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

__author__ = "David Amian <damian@emergya.com>"
__license__ = "GPL-2"

from pyactiveresource.activeresource import ActiveResource
import TimeEntry
import config
import aux
from xml.etree import ElementTree

class RedmineTEntries:
    """Represent class CargadorRedmine"""

    def __init__(self, host=None, hour_file=None):
        self.host = host
        self.hour_file = hour_file
        xmldoc = ElementTree.parse(hour_file)
        activities = xmldoc.findall('activity')
        for activity in activities:
            tags = activity.attrib['tags']
            tags = tags.split(',')
            issue_id = tags[0]
            comment = activity.attrib['description']
            comment = aux.decode(comment)
            time_s = activity.attrib['duration_minutes']
            time = round(float(time_s)/60, 2)
            time = aux.round_time(time)
            spent_on = activity.attrib['start_time'].split(' ')[0]
            time_entry = TimeEntry.TimeEntry({'issue_id':int(issue_id), 'hours':float(time), 'comments':str(comment), 'activity_id': config.ACTIVITY_ID, 'spent_on': spent_on})
            print "Inserting time entry for issue '" + str(issue_id) + "' with '" + str(time) + "' hours and '" + str(comment) + "' as comments, " + spent_on 
            time_entry.save()



if __name__ == '__main__':
    import os
    import sys
    choras = RedmineTEntries('test',sys.argv[1])
