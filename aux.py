# -*- coding: utf-8 -*-

import config

def round_time(time):
    time_s = str(time)
    t_split = time_s.split('.')
    number = t_split[0]
    decimal = t_split[1]
    if len(decimal) == 1:
        decimal = decimal + '0'
    if int(decimal) <= 12:
        decimal = "00"
    elif int(decimal) > 12 and int(decimal) <= 25:
        decimal = "25"
    elif int(decimal) > 25 and int(decimal) <= 37:
        decimal = "25"
    elif int(decimal) > 37 and int(decimal) <= 50:
        decimal = "50"
    elif int(decimal) > 50 and int(decimal) <= 62:
        decimal = "50"
    elif int(decimal) > 62 and int(decimal) <= 75:
        decimal = "75"
    elif int(decimal) > 75 and int(decimal) <= 87:
        decimal = "75"
    elif int(decimal) > 87 and int(decimal) <= 99:
        decimal = "00"
        number = str(int(number)+1)

    return int(number)+float(float(decimal)/100)

def decode(str_d):
    str_d = str_d.encode('utf8','replace')
    str_d = str_d.replace('\xe1','a') #á
    str_d = str_d.replace('\xe0','a') #à
    str_d = str_d.replace('\xe4','a') #ä
    str_d = str_d.replace('\xc1','A') #Á
    str_d = str_d.replace('\xc0','A') #À
    str_d = str_d.replace('\xc4','A') #Ä
    str_d = str_d.replace('\xe9','e') #é
    str_d = str_d.replace('\xe8','e') #è
    str_d = str_d.replace('\xeb','e') #ë
    str_d = str_d.replace('\xc9','E') #É
    str_d = str_d.replace('\xc8','E') #È
    str_d = str_d.replace('\xcb','E') #Ë
    str_d = str_d.replace('\xf3','o') #ó
    str_d = str_d.replace('\xf2','o') #ò
    str_d = str_d.replace('\xf6','o') #ö
    str_d = str_d.replace('\xd3','O') #Ó
    str_d = str_d.replace('\xd2','O') #Ò
    str_d = str_d.replace('\xd6','O') #Ö
    str_d = str_d.replace('\xed','i') #í
    str_d = str_d.replace('\xec','i') #ì
    str_d = str_d.replace('\xef','i') #ï
    str_d = str_d.replace('\xcd','I') #Í
    str_d = str_d.replace('\xcc','I') #Ì
    str_d = str_d.replace('\xcf','I') #Ï
    str_d = str_d.replace('\xfa','u') #ú
    str_d = str_d.replace('\xf9','u') #ù
    str_d = str_d.replace('\xfc','u') #ü
    str_d = str_d.replace('\xda','U') #Ú
    str_d = str_d.replace('\xd9','U') #Ù
    str_d = str_d.replace('\xdc','U') #Ü
    str_d = str_d.replace('\xbf','?')
    str_d = str_d.replace('\xa1','!')
    str_d = str_d.replace('\xf1','nh') #ñ
    str_d = str_d.replace('\xd1','NH') #Ñ
    str_d = str_d.replace('\xe7','cz;') #ç
    str_d = str_d.replace('\xc7','CZ') #Ç
    return str_d

